//! A generator for who buys for who in a secret santa.

#![windows_subsystem = "windows"]
#![warn(
    missing_docs,
    missing_debug_implementations,
    clippy::unwrap_used,
    clippy::pedantic
)]

use anyhow::Context as _;
use eframe::{
    egui::{
        self, Button, CentralPanel, Context, FontFamily::Proportional, FontId, Response,
        ScrollArea, SidePanel, TextEdit, TextStyle, TopBottomPanel, Ui,
    },
    run_native, NativeOptions,
};
use grouper::{LinkVec, Linkable};
use rand::{thread_rng, Rng};
use rfd::FileDialog;
use serde::{Deserialize, Serialize};
use std::{fmt::Write, fs::read_to_string};

fn main() -> eframe::Result {
    App::default().run()
}

#[derive(Default)]
struct App {
    groups: Groups,
    link_output: Option<String>,
    log: Vec<String>,
}

impl eframe::App for App {
    fn update(&mut self, ctx: &Context, _frame: &mut eframe::Frame) {
        if let Err(e) = self.try_update(ctx) {
            self.log.push(format!("Error: {e:?}"));
        }
    }
}

impl App {
    fn run(self) -> eframe::Result {
        run_native(
            "Secret Santa",
            NativeOptions::default(),
            Box::new(|cc| {
                cc.egui_ctx.style_mut(|style| {
                    style.text_styles = [
                        (TextStyle::Heading, FontId::new(30.0, Proportional)),
                        (TextStyle::Body, FontId::new(20.0, Proportional)),
                        (TextStyle::Button, FontId::new(20.0, Proportional)),
                    ]
                    .into();
                });
                Ok(Box::new(self))
            }),
        )
    }

    fn try_update(&mut self, ctx: &Context) -> anyhow::Result<()> {
        self.update_top_menu(ctx)?;
        self.update_log_panel(ctx);
        self.update_output_panel(ctx)?;
        self.update_people_panel(ctx);
        Ok(())
    }

    fn update_top_menu(&mut self, ctx: &Context) -> anyhow::Result<()> {
        TopBottomPanel::top("TopMenuPanel")
            .show(ctx, |ui| {
                egui::menu::bar(ui, |ui| {
                    let result = ui
                        .menu_button("File", |ui| {
                            if ui.button("Open People List").clicked() {
                                self.open_people_list()
                                    .context("failed to open people list from file")?;
                            }
                            if ui
                                .add_enabled(self.has_groups(), Button::new("Save People List"))
                                .clicked()
                            {
                                self.save_people_list()
                                    .context("failed to save people list to file")?;
                            }
                            if ui
                                .add_enabled(self.has_output(), Button::new("Save Output"))
                                .clicked()
                            {
                                self.save_output()
                                    .context("failed to save output to file")?;
                            }
                            Ok(())
                        })
                        .inner;
                    ui.menu_button("Edit", |ui| {
                        if ui.button("Clear").clicked() {
                            self.clear();
                        }
                    });
                    result
                })
                .inner
            })
            .inner
            .unwrap_or(Ok(()))
    }

    fn update_log_panel(&mut self, ctx: &Context) {
        TopBottomPanel::bottom("LogPanel")
            .resizable(true)
            .show(ctx, |ui| {
                ui.heading("Error Log");
                ScrollArea::vertical().auto_shrink(false).show(ui, |ui| {
                    for msg in self.log.iter_mut().rev() {
                        ui.add(
                            TextEdit::multiline(msg)
                                .interactive(false)
                                .desired_width(f32::INFINITY),
                        )
                        .context_menu(|ui| {
                            if ui.button("Copy To Clipboard").clicked() {
                                ui.ctx().copy_text(msg.clone());
                            }
                        });
                    }
                });
            });
    }

    fn update_output_panel(&mut self, ctx: &Context) -> anyhow::Result<()> {
        SidePanel::right("LinksPanel")
            .exact_width(ctx.screen_rect().width() / 2.0)
            .resizable(false)
            .show(ctx, |ui| {
                ui.vertical(|ui| {
                    if ui.button("Generate").clicked() {
                        self.generate_output()
                            .context("failed to generate output")?;
                    }

                    if let Some(link_output) = &mut self.link_output {
                        ui.separator();
                        ui.add(
                            TextEdit::multiline(link_output)
                                .interactive(false)
                                .desired_width(f32::INFINITY),
                        )
                        .context_menu(|ui| {
                            if ui.button("Copy To Clipboard").clicked() {
                                ui.ctx().copy_text(link_output.clone());
                            }
                        });
                    }
                    Ok(())
                })
                .inner
            })
            .inner
    }

    fn update_people_panel(&mut self, ctx: &Context) {
        CentralPanel::default().show(ctx, |ui| {
            self.groups.show(ui);
        });
    }

    fn generate_output(&mut self) -> anyhow::Result<()> {
        if !self.has_people() {
            self.link_output = None;
            return Ok(());
        }

        let rng = |range| thread_rng().gen_range(range);
        let items = self.groups.indices();
        let links = LinkVec::new(items, 2, &rng).context("failed to generate links")?;

        let mut output = String::new();
        for (a_i, b_i) in links.iter() {
            let a_g = &self.groups.groups[a_i.group];
            let a_p = &a_g.people[a_i.person];
            let a_str = format!("{} {}", a_p.name, a_g.name);

            let b_str = match b_i {
                Some(b_i) => {
                    let b_g = &self.groups.groups[b_i.group];
                    let b_p = &b_g.people[b_i.person];
                    format!("{} {}", b_p.name, b_g.name)
                }
                None => String::from("Nobody"),
            };

            writeln!(&mut output, "{a_str} buys for {b_str}")
                .expect("writing to string cannot fail");
        }

        self.link_output = Some(output);
        Ok(())
    }

    fn open_people_list(&mut self) -> anyhow::Result<()> {
        let Some(path) = FileDialog::new().add_filter("json", &["json"]).pick_file() else {
            return Ok(());
        };
        let contents = read_to_string(path).context("failed to read file contents")?;
        self.groups =
            serde_json::from_str(&contents).context("failed to parse people list from json")?;
        Ok(())
    }

    fn save_people_list(&self) -> anyhow::Result<()> {
        let Some(path) = FileDialog::new().add_filter("json", &["json"]).save_file() else {
            return Ok(());
        };
        let contents = serde_json::to_string_pretty(&self.groups)
            .context("failed to serialize people list to json")?;
        std::fs::write(path, contents).context("failed to write to the file")?;
        Ok(())
    }

    fn save_output(&self) -> anyhow::Result<()> {
        let Some(path) = FileDialog::new().add_filter("text", &["txt"]).save_file() else {
            return Ok(());
        };
        let contents = self.link_output.as_deref().unwrap_or_default();
        std::fs::write(path, contents).context("failed to write to the file")?;
        Ok(())
    }

    fn clear(&mut self) {
        self.groups = Groups::default();
        self.link_output = None;
    }

    fn has_groups(&self) -> bool {
        !self.groups.groups.is_empty()
    }

    fn has_people(&self) -> bool {
        self.groups
            .groups
            .iter()
            .map(|g| g.people.len())
            .sum::<usize>()
            > 0
    }

    fn has_output(&self) -> bool {
        self.link_output.is_some()
    }
}

#[derive(Default, Clone, Deserialize, Serialize)]
struct Groups {
    groups: Vec<Group>,
}

impl Groups {
    fn show(&mut self, ui: &mut Ui) {
        ui.vertical(|ui| {
            let mut to_delete = None;
            for (i, group) in self.groups.iter_mut().enumerate() {
                if group.show(ui).clicked() {
                    to_delete = Some(i);
                }
                ui.separator();
            }
            if let Some(i) = to_delete {
                self.groups.remove(i);
            }

            if ui.button("Add Group").clicked() {
                self.groups.push(Group {
                    name: String::from("New Group"),
                    people: Vec::new(),
                });
            }
        });
    }

    fn indices(&self) -> Vec<PersonIndex> {
        self.groups
            .iter()
            .enumerate()
            .flat_map(|(gi, group)| {
                group
                    .people
                    .iter()
                    .enumerate()
                    .map(move |(pi, _)| PersonIndex {
                        group: gi,
                        person: pi,
                    })
            })
            .collect()
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct Group {
    name: String,
    people: Vec<Person>,
}

impl Group {
    fn show(&mut self, ui: &mut Ui) -> Response {
        let response = ui
            .horizontal(|ui| {
                ui.add(TextEdit::singleline(&mut self.name).font(TextStyle::Heading));
                ui.button("Delete Group")
            })
            .inner;

        let mut to_delete = None;
        for (i, person) in self.people.iter_mut().enumerate() {
            if person.show(ui).clicked() {
                to_delete = Some(i);
            }
        }
        if let Some(i) = to_delete {
            self.people.remove(i);
        }

        if ui.button("Add Person").clicked() {
            self.people.push(Person {
                name: String::from("New Person"),
            });
        }

        response
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct Person {
    name: String,
}

impl Person {
    fn show(&mut self, ui: &mut Ui) -> Response {
        ui.horizontal(|ui| {
            ui.label("-");
            ui.text_edit_singleline(&mut self.name);

            ui.button("Delete Person")
        })
        .inner
    }
}

#[derive(Debug)]
struct PersonIndex {
    group: usize,
    person: usize,
}

impl Linkable for PersonIndex {
    fn can_link_to(&self, to: &Self) -> bool {
        self.group != to.group
    }
}
